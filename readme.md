# Java implementation for "Rock Paper Scissors" game.

## Description
Here you can find an implementation of the famous game "Rock Paper Scissors".The rules are simple: two opponents simultaneously choose rock, paper, or scissors. Paper beats rock, rock beats scissors, and scissors beats paper.If opponents makes true-random choices the probability of win is equal for all players. However human beings are predictable and there are a lot of different strategies how to win more often.
Two strategies were implemented at this system:

* **Psychological** With according to [this investigation](https://arxiv.org/pdf/1404.5199v1.pdf) (made by chinese guys) there are several simple rules: If you win, show at the next time last opponent sign. If you lose, show at the next time, sign which beat last opponent sign. And vise versa if you find out that opponent knows this strategy.

* **Template** This strategy based on ['Markov chains'](https://en.wikipedia.org/wiki/Markov_chain) probabilistic theory. This is a self-learned strategy. It tries to predict an opponent bid, based on  historical data about last games.

This application provides an user interface (cli) to play this game against computer (random mix of strategies declared above).
Also you can easily implement your own strategies or modify current.

## Prerequisites
You need JDK8 or higher installed

### Build
Build the project from project root directory :
>./gradlew clean build

### Launch
If you want to run a user shell (cli) just execute jar file without any input arguments
>java -jar ./build/libs/spr-game-1.0.jar
