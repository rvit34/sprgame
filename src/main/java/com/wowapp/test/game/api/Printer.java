package com.wowapp.test.game.api;

import java.io.Closeable;

public interface Printer extends Closeable {

    void printLine(String line);

    void printErrorLine(String errorMsg);

    void printFormattedLine(String line, Object... args);

    void printStats(UserStats stats);

}
