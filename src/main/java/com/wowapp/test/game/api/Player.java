package com.wowapp.test.game.api;

import com.wowapp.test.game.core.Figure;

@FunctionalInterface
public interface Player {

   Figure play();

}
