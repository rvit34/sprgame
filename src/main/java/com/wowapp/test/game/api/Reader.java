package com.wowapp.test.game.api;

import java.io.Closeable;

public interface Reader extends Closeable {

    String readNextLine();
}
