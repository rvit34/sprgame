package com.wowapp.test.game.api;

import com.wowapp.test.game.core.GameEntry;

import java.util.Optional;

public interface GameTrack {

    void add(GameEntry game);

    Optional<GameEntry> getLastGame();
}
