package com.wowapp.test.game.api;

public interface UserStats {

    int winsCount();

    int lossesCount();

    int tiesCount();

    int totalGamesCount();
}
