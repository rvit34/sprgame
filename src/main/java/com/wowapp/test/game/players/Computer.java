package com.wowapp.test.game.players;

import com.wowapp.test.game.core.Figure;
import com.wowapp.test.game.api.GameStrategy;
import com.wowapp.test.game.api.Player;

import java.util.Arrays;
import java.util.List;
import java.util.Random;

public class Computer implements Player {

    private final List<GameStrategy> strategies;
    private final Random random = new Random();

    public Computer(GameStrategy... strategies) { this.strategies = Arrays.asList(strategies); }

    @Override
    public Figure play() {
        // to make user confused we apply a mix of strategies here
        return strategies.get(random.nextInt(strategies.size())).play();
    }
}
