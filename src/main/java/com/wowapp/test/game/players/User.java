package com.wowapp.test.game.players;

import com.wowapp.test.game.api.Player;
import com.wowapp.test.game.api.Printer;
import com.wowapp.test.game.api.Reader;
import com.wowapp.test.game.core.Figure;

import java.util.Arrays;
import java.util.stream.Stream;

public class User implements Player {

    private final Reader reader;
    private final Printer printer;

    private final String figures = Arrays.toString(Figure.values());
    private final String hint;

    public User(Reader reader, Printer printer) {
        this.reader = reader;
        this.printer = printer;
        this.hint = Stream.of(Figure.values())
                .map(f -> new String(new char[]{f.name().toUpperCase().charAt(0)}))
                .reduce((ch1, ch2) -> ch1 + "/" + ch2)
                .orElse("");
    }

    @Override
    public Figure play() {

        printer.printLine(String.format("Choose figure: %s. Type the first letter of desired figure (%s):", figures, hint));

        String userInput = reader.readNextLine();
        if (userInput == null || userInput.isEmpty()) {
            printer.printErrorLine("Incorrect input. Try again");
            return play();
        }
        char firstLetter = userInput.toUpperCase().charAt(0);
        return Figure.getByFirstLetter(firstLetter).orElseGet(() -> {
            printer.printErrorLine("Incorrect input. Try again");
            return play();
        });
    }
}
