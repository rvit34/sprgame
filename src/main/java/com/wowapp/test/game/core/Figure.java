package com.wowapp.test.game.core;

import java.util.Optional;
import java.util.Random;

public enum Figure {

    SCISSORS, PAPER, ROCK;

    /**
     * compares this figure to another, passed through argument method
     * @param anotherFigure - the figure to compare
     * @return 0 - if figures are equal, 1 if this figure beats another, otherwise -1
     */
    public int compare(Figure anotherFigure) {

        if (this == anotherFigure) {
            return 0;
        }

        switch (this) {
            case SCISSORS: return anotherFigure == PAPER ? 1 : -1;
            case ROCK: return anotherFigure == SCISSORS ? 1 : -1;
            case PAPER: return anotherFigure == ROCK ? 1 : -1;
        }

        return 0;
    }

    public Figure getStrongerFigure() {

        switch (this) {
            case SCISSORS: return ROCK;
            case PAPER: return SCISSORS;
            case ROCK: return PAPER;
        }

        return this;
    }

    public static Optional<Figure> getByFirstLetter(char letter) {
        for (Figure figure : values()) {
            if (figure.name().toUpperCase().charAt(0) == letter) {
                return Optional.of(figure);
            }
        }
        return Optional.empty();
    }

    private static final Random random = new Random();

    public static Figure getRandomFigure() {
        return values()[random.nextInt(values().length)];
    }
}
