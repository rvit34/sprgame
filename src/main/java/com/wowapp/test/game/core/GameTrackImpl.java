package com.wowapp.test.game.core;

import com.wowapp.test.game.api.GameTrack;
import com.wowapp.test.game.api.UserStats;

import java.util.*;

public class GameTrackImpl implements GameTrack, UserStats {

    private final LinkedList<GameEntry> track = new LinkedList<>();

    @Override
    public void add(GameEntry game) { track.add(game); }

    @Override
    public Optional<GameEntry> getLastGame() {
        return Optional.ofNullable(track.isEmpty() ? null : track.getLast());
    }

    @Override
    public int winsCount() {
       return (int) track.parallelStream().filter(GameEntry::isUserWon).count();
    }

    @Override
    public int lossesCount() {
        return (int) track.parallelStream().filter(GameEntry::isComputerWon).count();
    }

    @Override
    public int tiesCount() {
        return (int) track.parallelStream().filter(GameEntry::isTie).count();
    }

    @Override
    public int totalGamesCount() {
        return track.size();
    }

}
