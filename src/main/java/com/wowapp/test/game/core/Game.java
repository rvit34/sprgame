package com.wowapp.test.game.core;

import com.wowapp.test.game.api.*;
import lombok.AllArgsConstructor;

import java.util.List;

@AllArgsConstructor
public class Game {

    private final Player user;
    private final Player computer;
    private final UserStats userStats;
    private final List<GameTrack> gameTracks;
    private final Printer printer;
    private final Reader reader;

    public void start() {
        start("Welcome to the game Scissors, Paper, Rock.");
    }

    private void start(String welcomeMsg) {
        printer.printLine(welcomeMsg + " Please, choose one of the following options:");
        printer.printLine("1: play a new game\n2: print game stats\n3: quit");
        handleInput();
    }

    private void handleInput() {
        String userInput = reader.readNextLine();
        if (userInput == null || userInput.isEmpty()) {
            printer.printErrorLine("Wrong input. Only 1-3 is available. Try again");
            handleInput();
        } else {
            char option = userInput.charAt(0);
            switch (option) {
                case '1':
                    play();
                    break;
                case '2':
                    printStats();
                    start("");
                    break;
                case '3':
                    quit();
                    break;
                default:
                    printer.printErrorLine("Wrong input. Only 1-3 is available. Try again");
                    handleInput();
            }
        }
    }

    private void play() {

        Figure userFigure = user.play();
        Figure computerFigure = computer.play();

        printer.printLine(String.format("You played %s", userFigure));
        printer.printLine(String.format("Computer played %s", computerFigure));

        final int result = userFigure.compare(computerFigure);
        switch (result) {
           case 0:
               printer.printLine("Tie");
               break;
           case 1:
               printer.printLine(userFigure + " beats " + computerFigure + ". So you won!");
               break;
           case -1:
               printer.printLine(computerFigure + " beats " + userFigure + ". So computer won!");
               break;
           default:
               printer.printErrorLine("Something went wrong in figures comparation. Let developer know about it.");
        }

        gameTracks.forEach(gameTrack -> gameTrack.add(new GameEntry(userFigure, computerFigure)));
        tryToReplay();
    }

    private void tryToReplay() {

        printer.printLine("Would you like to play once more (Y/N) ?");
        String userInput = reader.readNextLine();
        if (userInput == null || userInput.isEmpty()) {
            printer.printErrorLine("Incorrect input");
            tryToReplay();
        } else {
            userInput = userInput.toUpperCase();
            if (userInput.startsWith("Y")) {
                play();
            } else if (userInput.startsWith("N")){
                start("");
            } else {
                tryToReplay();
            }
        }
    }

    private void quit() {
        printer.printLine("Good bie");
        System.exit(0);
    }

    private void printStats() {
        printer.printStats(userStats);
    }

}
