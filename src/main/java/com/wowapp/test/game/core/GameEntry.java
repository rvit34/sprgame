package com.wowapp.test.game.core;

import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;

@AllArgsConstructor
@Getter
@EqualsAndHashCode
public class GameEntry {

    private final Figure userFigure;
    private final Figure computerFigure;

    public boolean isUserWon() {
        return userFigure.compare(computerFigure) == 1;
    }

    public boolean isComputerWon() {
        return userFigure.compare(computerFigure) == -1;
    }

    public boolean isTie() {
        return computerFigure.compare(userFigure) == 0;
    }

}
