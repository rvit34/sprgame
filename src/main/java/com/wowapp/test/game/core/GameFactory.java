package com.wowapp.test.game.core;

import com.wowapp.test.game.api.*;
import com.wowapp.test.game.io.ConsolePrinter;
import com.wowapp.test.game.io.ConsoleReader;
import com.wowapp.test.game.players.Computer;
import com.wowapp.test.game.players.User;
import com.wowapp.test.game.strategies.PsychologicalStrategy;
import com.wowapp.test.game.strategies.TemplateStrategy;

import java.util.Arrays;

public class GameFactory {

    public Game createConsoleGame() {

        Printer printer = new ConsolePrinter();
        Reader reader = new ConsoleReader();

        GameTrack gameTrack = new GameTrackImpl();
        UserStats userStats = (UserStats) gameTrack;

        GameStrategy templateStrategy = new TemplateStrategy(3);
        GameTrack templateGameTrack = (GameTrack) templateStrategy;

        Player computer = new Computer(templateStrategy, new PsychologicalStrategy(gameTrack));
        Player user = new User(reader, printer);

        return new Game(user, computer, userStats, Arrays.asList(gameTrack, templateGameTrack), printer, reader);

    }


}
