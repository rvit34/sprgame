package com.wowapp.test.game.core;

public class GameLauncher {

    public static void main(String[] args) {
        GameFactory gameFactory = new GameFactory();
        Game consoleGame = gameFactory.createConsoleGame();
        consoleGame.start();
    }
}
