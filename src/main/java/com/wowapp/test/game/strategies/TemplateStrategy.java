package com.wowapp.test.game.strategies;

import com.wowapp.test.game.api.GameStrategy;
import com.wowapp.test.game.api.GameTrack;
import com.wowapp.test.game.core.Figure;
import com.wowapp.test.game.core.GameEntry;

import java.util.*;

/**
 * This strategy is based on observation that human might repeat moves during the game.<br>
 * So we need to extract a part of last track as a template and try to find occurrences in a whole track before.
 */
public class TemplateStrategy implements GameStrategy, GameTrack {

    private final List<GameEntry> track = new ArrayList<>();
    private final Map<Circuit, Circuit> circuits;

    private final int maxSeqSize; // the length of seeking sequence(template)

    public TemplateStrategy(int maxSeqSize) {
        this.maxSeqSize = maxSeqSize;
        this.circuits = new HashMap<>((int) Math.pow(Figure.values().length, maxSeqSize));
    }

    @Override
    public Figure play() {

        // here let's select all built circuits for last maxSeqSize-moves in track
        Set<Circuit> selectedCircuits = new HashSet<>(maxSeqSize);
        int end = track.size() - 1;
        int start = end - maxSeqSize + 1;

        selectCircuitsForPlay(start, end, selectedCircuits);

        if (selectedCircuits.isEmpty()) {
            return Figure.getRandomFigure();
        }

        // then select the best one based on max weight
        Circuit bestCircuit = selectedCircuits.stream()
                .max(Comparator.comparingInt(Circuit::getWeight)).get(); //don't worry, here optional is always true

        // finally select stronger figure than that is best based on counter of user selects in chain
        return bestCircuit.getBestMatch().map(Figure::getStrongerFigure).orElse(Figure.getRandomFigure());
    }

    @Override
    public void add(GameEntry game) {
        track.add(game);
        if (track.size() == 1) {
            return;
        }
        int end = track.size() - 1;
        int start = end - maxSeqSize;
        addCircuits(game.getUserFigure(), start, end);
    }

    private void selectCircuitsForPlay(int start, int end, Set<Circuit> selected) {

        if (start >= end) {
            return;
        }

        int s = start;
        int i = 0;
        GameEntry[] sequence = new GameEntry[maxSeqSize];
        while (start <= end) {
            if (start >= 0) {
                sequence[i] = track.get(start);
            }
            start++;
            i++;
        }
        Circuit newCircuit = new Circuit(sequence);

        Circuit foundCircuit = circuits.get(newCircuit);
        if (foundCircuit != null) {
            selected.add(foundCircuit);
        }

        selectCircuitsForPlay(s+1, end, selected);
    }

    private void addCircuits(Figure userFigure, int start, int end) {

        if (start >= end) {
            return;
        }

        int s = start;
        int i = 0;
        GameEntry[] sequence = new GameEntry[maxSeqSize];
        while (start < end) {
            if (start >= 0) {
                sequence[i] = track.get(start);
            }
            start++;
            i++;
        }

        Circuit newCircuit = new Circuit(sequence);
        newCircuit.userSelected(userFigure);

        Circuit foundCircuit = circuits.get(newCircuit);
        if (foundCircuit == null) {
            circuits.put(newCircuit, newCircuit); // add new
        } else {                                  // or
            foundCircuit.userSelected(userFigure); // increment counter
        }

        addCircuits(userFigure, s + 1, end); // also add all another less length circuits
    }

    @Override
    public Optional<GameEntry> getLastGame() {
        return Optional.ofNullable(track.get(track.size() - 1));
    }

    private class Circuit {

        private final GameEntry[] sequence; // it is a part of track (template)

        private final EnumMap<Figure, Integer> userSelects = new EnumMap<>(Figure.class); // it represents the user next choice after template and counter

        public Circuit(GameEntry[] sequence) {
           this.sequence = sequence;
        }

        public void userSelected(Figure userFigure) {
            Integer prevSelects = userSelects.get(userFigure);
            userSelects.put(userFigure, prevSelects == null ? 1 : prevSelects + 1);
        }

        public Optional<Figure> getBestMatch() {
            return userSelects.entrySet().stream()
                    .max(Comparator.comparing(Map.Entry::getValue))
                    .map(Map.Entry::getKey);
        }

        public int getWeight() {

            int weight = maxSeqSize;

            // not completed sequence have less weight
            for (GameEntry entry : sequence) {
                if (entry == null) {
                    weight--;
                }
            }

            int maxSelects = userSelects.values().stream().max(Integer::compareTo).orElse(0);

            return weight * maxSelects;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;
            Circuit circuit = (Circuit) o;
            return Arrays.equals(sequence, circuit.sequence);
        }

        @Override
        public int hashCode() {
            return Arrays.hashCode(sequence);
        }
    }

}
