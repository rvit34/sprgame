package com.wowapp.test.game.strategies;

import com.wowapp.test.game.api.GameStrategy;
import com.wowapp.test.game.api.GameTrack;
import com.wowapp.test.game.core.Figure;
import com.wowapp.test.game.core.GameEntry;
import lombok.AllArgsConstructor;

import java.util.Random;

/**
 * Psychological strategy is based on two observable things.<br>
 * Usually when a human has success he/she is not going to change anything.<br>
 * Otherwise a human is most likely to pick a stronger figure up on the next move.<br>
 **/
@AllArgsConstructor
public class PsychologicalStrategy implements GameStrategy {

    private final GameTrack gameTrack;
    private final Random random = new Random();

    @Override
    public Figure play() {
        return gameTrack.getLastGame().map(lastGame -> {
            Figure userFigure = lastGame.getUserFigure();
            return lastGame.isUserWon() ?
                    userFigure.getStrongerFigure() : chooseStrongerFigure(lastGame);
        }).orElse(Figure.getRandomFigure());
    }

    private Figure chooseStrongerFigure(GameEntry game) {
        return (random.nextInt(2) == 0 ?
                game.getUserFigure().getStrongerFigure().getStrongerFigure() :
                game.getComputerFigure().getStrongerFigure().getStrongerFigure());
    }

}
