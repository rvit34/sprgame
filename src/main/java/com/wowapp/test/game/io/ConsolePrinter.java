package com.wowapp.test.game.io;

import com.wowapp.test.game.api.Printer;
import com.wowapp.test.game.api.UserStats;

import java.io.PrintWriter;

public class ConsolePrinter implements Printer {

    private final PrintWriter writer = new PrintWriter(System.out);
    private final PrintWriter errWriter = new PrintWriter(System.err);

    @Override
    public void printLine(String line) {
       printLine(line, true);
    }

    @Override
    public void printFormattedLine(String line, Object... args) {
        printFormattedLineInternall(line, true, args);
    }

    @Override
    public void printErrorLine(String errorMsg) {
        errWriter.println(errorMsg);
        errWriter.flush();
    }

    @Override
    public void printStats(UserStats stats) {

        writer.print("+");
        printDashes(38);
        writer.println("+");

        printFormattedLineInternall("|  %5s  |  %6s  |  %4s  |  %5s |\n", false,
                "WINS", "LOSSES", "TIE", "TOTAL");

        writer.print("|");
        printDashes(38);
        writer.print("|");

        printFormattedLineInternall("\n|  %5d  |  %6d  |  %5d | %6d |\n", false,
                stats.winsCount(), stats.lossesCount(), stats.tiesCount(), stats.totalGamesCount());

        writer.print("+");
        printDashes(38);
        writer.println("+");
        writer.flush();
    }

    @Override
    public void close() {
        writer.close();
        errWriter.close();
    }

    private void printLine(String line, boolean flushImmediately) {
        writer.println(line);
        if (flushImmediately) {
            writer.flush();
        }
    }

    private void printFormattedLineInternall(String line, boolean flushImmediately, Object... args) {
        writer.printf(line, args);
        if (flushImmediately) {
            writer.flush();
        }
    }

    private void printDashes(int count) {
        for (int i = 0; i < count; i++) {
            writer.print("-");
        }
    }

}
