package com.wowapp.test.game.strategies;

import com.wowapp.test.game.api.GameTrack;
import com.wowapp.test.game.core.Figure;
import com.wowapp.test.game.core.GameEntry;
import com.wowapp.test.game.core.GameTrackImpl;
import org.junit.Assert;
import org.junit.Test;

import static org.junit.Assert.*;

public class PsychologicalStrategyTest {

    private GameTrack gameTrack = new GameTrackImpl();
    private PsychologicalStrategy psyStrategy = new PsychologicalStrategy(gameTrack);

    @Test
    public void play() {

        gameTrack.add(new GameEntry(Figure.SCISSORS, Figure.PAPER));
        // here is suggested than user will select SCISSORS again
        Assert.assertTrue(psyStrategy.play() == Figure.ROCK);

        gameTrack.add(new GameEntry(Figure.SCISSORS, Figure.ROCK));
        // here is suggested than user will change figure to ROCK or PAPER, so strategy should play PAPER or SCISSORS
        Figure psyFigure = psyStrategy.play();
        Assert.assertTrue(psyFigure == Figure.PAPER || psyFigure == Figure.SCISSORS);
    }
}