package com.wowapp.test.game.strategies;

import com.wowapp.test.game.core.Figure;
import com.wowapp.test.game.core.GameEntry;
import org.junit.Before;
import org.junit.Test;

import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.assertTrue;

public class TemplateStrategyTest {

    private TemplateStrategy templateStrategy = new TemplateStrategy(3);

    @Before
    public void setUp(){

        List<GameEntry> gameTrack = Arrays.asList(
            new GameEntry(Figure.PAPER, Figure.ROCK),
            new GameEntry(Figure.SCISSORS, Figure.PAPER),
            new GameEntry(Figure.ROCK, Figure.SCISSORS),
            new GameEntry(Figure.SCISSORS, Figure.PAPER),

            new GameEntry(Figure.PAPER, Figure.PAPER),
            new GameEntry(Figure.PAPER, Figure.PAPER),
            new GameEntry(Figure.PAPER, Figure.PAPER),

            new GameEntry(Figure.PAPER, Figure.ROCK),
            new GameEntry(Figure.SCISSORS, Figure.PAPER),
            new GameEntry(Figure.ROCK, Figure.SCISSORS),
            new GameEntry(Figure.SCISSORS, Figure.PAPER)
            // it's suggested that next user will select PAPER again, so the strategy must select SCISSORS
        );

        gameTrack.forEach(templateStrategy::add);
    }

    @Test
    public void testPlay() {

        Figure figure = templateStrategy.play();
        assertTrue(figure == Figure.SCISSORS);
    }
}