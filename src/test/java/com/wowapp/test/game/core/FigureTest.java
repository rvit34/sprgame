package com.wowapp.test.game.core;

import org.junit.Assert;
import org.junit.Test;

import java.util.Optional;

import static org.junit.Assert.*;

public class FigureTest {

    @Test
    public void compareTest() {

        Assert.assertTrue(Figure.PAPER.compare(Figure.PAPER) == 0);
        Assert.assertTrue(Figure.SCISSORS.compare(Figure.SCISSORS) == 0);
        Assert.assertTrue(Figure.ROCK.compare(Figure.ROCK) == 0);

        Assert.assertTrue(Figure.SCISSORS.compare(Figure.PAPER) == 1);
        Assert.assertTrue(Figure.PAPER.compare(Figure.SCISSORS) == -1);

        Assert.assertTrue(Figure.ROCK.compare(Figure.SCISSORS) == 1);
        Assert.assertTrue(Figure.SCISSORS.compare(Figure.ROCK) == -1);

        Assert.assertTrue(Figure.PAPER.compare(Figure.ROCK) == 1);
        Assert.assertTrue(Figure.ROCK.compare(Figure.PAPER) == -1);
    }

    @Test
    public void getStrongerFigureTest() {
        Assert.assertTrue(Figure.ROCK == Figure.SCISSORS.getStrongerFigure());
        Assert.assertTrue(Figure.PAPER == Figure.ROCK.getStrongerFigure());
        Assert.assertTrue(Figure.SCISSORS == Figure.PAPER.getStrongerFigure());
    }

    @Test
    public void getFigureByFirstLetterTest() {

        Optional<Figure> mightFigure = Figure.getByFirstLetter('P');
        Assert.assertTrue(mightFigure.isPresent() && mightFigure.get() == Figure.PAPER);

        mightFigure = Figure.getByFirstLetter('R');
        Assert.assertTrue(mightFigure.isPresent() && mightFigure.get() == Figure.ROCK);

        mightFigure = Figure.getByFirstLetter('S');
        Assert.assertTrue(mightFigure.isPresent() && mightFigure.get() == Figure.SCISSORS);
    }
}